﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RearrangePageNavigation2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DetailsPage : Page
    {
        private readonly SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();

        public DetailsPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ConnectedAnimationService.GetForCurrentView().DefaultDuration = TimeSpan.FromSeconds(3);

            var animation = ConnectedAnimationService.GetForCurrentView().GetAnimation("Image");
            if (animation != null)
            {
                this.DetailsImage.Opacity = 0;
                this.DetailsImage.ImageOpened += (sender, args) =>
                {
                    this.DetailsImage.Opacity = 1;
                    animation.TryStart(this.DetailsImage);
                };
            }

            var animationText = ConnectedAnimationService.GetForCurrentView().GetAnimation("Text");
            if (animationText != null)
            {
                if (e.Parameter != null)
                {
                    this.DetailsText.Text = e.Parameter.ToString();
                    animationText.TryStart(this.DetailsText);
                }
            }

            this.systemNavigationManager.BackRequested += OnBackRequested;
            this.systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            this.systemNavigationManager.BackRequested -= OnBackRequested;
            this.systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (e.Handled)
            {
                return;
            }

            ConnectedAnimationService.GetForCurrentView().PrepareToAnimate("Image", this.DetailsImage);
            ConnectedAnimationService.GetForCurrentView().PrepareToAnimate("Text", this.DetailsText);

            e.Handled = true;
            Frame.GoBack();
        }
    }
}
