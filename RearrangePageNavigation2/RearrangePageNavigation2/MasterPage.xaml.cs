﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RearrangePageNavigation2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MasterPage : Page
    {
        private static string navigatedImageItem;

        public MasterPage()
        {
            this.InitializeComponent();

            DataContext = new ViewModel();

            Loaded += OnLoaded;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Don't use vertical entrance animation with connected animation
            if (e.NavigationMode == NavigationMode.Back)
            {
                this.EntranceTransition.FromVerticalOffset = 0;
            }

            //Hide the back button on the list page as there is no where to go back to. 
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            // May be able to perform backwards Connected Animation
            if ((ViewModel != null) && (!string.IsNullOrEmpty(navigatedImageItem)))
            {
                var item = ViewModel.Items.First(x => x == navigatedImageItem);

                this.MainListView.ScrollIntoView(item, ScrollIntoViewAlignment.Default);
                this.MainListView.UpdateLayout();

                var container = this.MainListView.ContainerFromItem(item) as ListViewItem;
                if (container != null)
                {
                    var animation = ConnectedAnimationService.GetForCurrentView().GetAnimation("Image");
                    if (animation != null)
                    {
                        var root = (FrameworkElement) container.ContentTemplateRoot;
                        var image = (Image) root.FindName("ItemImage");

                        // Wait for image opened. In future Insider Preview releases, this won't be necessary.
                        image.Opacity = 0;
                        image.ImageOpened += (s, args) =>
                        {
                            image.Opacity = 1;
                            animation.TryStart(image);
                        };
                    }
                    else
                    {
                        animation.Cancel();
                    }

                    var animationText = ConnectedAnimationService.GetForCurrentView().GetAnimation("Text");
                    if (animationText != null)
                    {
                        var root = (FrameworkElement)container.ContentTemplateRoot;
                        var textBlock = (TextBlock)root.FindName("ItemText");
                        animationText.TryStart(textBlock);
                    }
                    else
                    {
                        animationText.Cancel();
                    }
                }
            }
        }

        private void OnItemClick(object sender, ItemClickEventArgs e)
        {
            navigatedImageItem = e.ClickedItem as string;

            if (navigatedImageItem == null)
            {
                return;
            }

            var container = this.MainListView.ContainerFromItem(e.ClickedItem) as ListViewItem;
            if (container != null)
            {
                var root = (FrameworkElement)container.ContentTemplateRoot;
                var image = (UIElement)root.FindName("ItemImage");

                ConnectedAnimationService.GetForCurrentView().PrepareToAnimate("Image", image);

                var text = (UIElement) root.FindName("ItemText");
                ConnectedAnimationService.GetForCurrentView().PrepareToAnimate("Text", text);

                // Add a fade out effect
                Transitions = new TransitionCollection {new ContentThemeTransition()};

                Frame.Navigate(typeof(DetailsPage), navigatedImageItem);
            }
        }

        public ViewModel ViewModel => DataContext as ViewModel;
    }
}
